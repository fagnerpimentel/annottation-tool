#-------------------------------------------------
#
# Project created by QtCreator 2014-04-22T11:28:26
#
#-------------------------------------------------

include(Vision/Vision.pri)
include(Interface/Interface.pri)
include(package_bgs/package_bgs.pri)

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Annotation
TEMPLATE = app


SOURCES += main.cpp \
    util.cpp

HEADERS  += \
    util.h

INCLUDEPATH += /usr/local/include/
INCLUDEPATH += /usr/local/include/opencv2/
INCLUDEPATH += /usr/local/include/opencv/

LIBS += -L/usr/local/lib -lopencv_core -lopencv_highgui -lopencv_imgproc -lopencv_features2d -lopencv_calib3d
