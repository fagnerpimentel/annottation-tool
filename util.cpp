#include "util.h"

QImage Mat2QImage(cv::Mat &src)
{
//    cv::cvtColor(src, src, CV_RGB2BGR);
    return QImage((uchar*) src.data, src.cols, src.rows, src.step,QImage::Format_RGB888);
}

cv::Mat Equalization(const cv::Mat& inputImage)
{
    if(inputImage.channels() >= 3)
    {
        std::vector<cv::Mat> channels;
        cv::split(inputImage,channels);
        cv::Mat B,G,R;

        cv::equalizeHist( channels[0], B );
        cv::equalizeHist( channels[1], G );
        cv::equalizeHist( channels[2], R );
        std::vector<cv::Mat> combined;
        combined.push_back(B);
        combined.push_back(G);
        combined.push_back(R);
        cv::Mat result;
        cv::merge(combined,result);
        return result;
    }
    return inputImage;
}
