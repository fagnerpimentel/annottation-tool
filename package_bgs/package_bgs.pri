INCLUDEPATH += $$PWD
DEPENDPATH += $$PWD
SOURCES += \  
    package_bgs/dp/AdaptiveMedianBGS.cpp \
    package_bgs/dp/DPAdaptiveMedianBGS.cpp \
    package_bgs/dp/DPEigenbackgroundBGS.cpp \
    package_bgs/dp/DPGrimsonGMMBGS.cpp \
    package_bgs/dp/DPMeanBGS.cpp \
    package_bgs/dp/DPPratiMediodBGS.cpp \
    package_bgs/dp/DPTextureBGS.cpp \
    package_bgs/dp/DPWrenGABGS.cpp \
    package_bgs/dp/DPZivkovicAGMMBGS.cpp \
    package_bgs/dp/Eigenbackground.cpp \
    package_bgs/dp/Error.cpp \
    package_bgs/dp/GrimsonGMM.cpp \
    package_bgs/dp/Image.cpp \
    package_bgs/dp/MeanBGS.cpp \
    package_bgs/dp/PratiMediodBGS.cpp \
    package_bgs/dp/TextureBGS.cpp \
    package_bgs/dp/WrenGA.cpp \
    package_bgs/dp/ZivkovicAGMM.cpp


HEADERS += \
    package_bgs/dp/AdaptiveMedianBGS.h \
    package_bgs/dp/Bgs.h \
    package_bgs/dp/BgsParams.h \
    package_bgs/dp/DPAdaptiveMedianBGS.h \
    package_bgs/dp/DPEigenbackgroundBGS.h \
    package_bgs/dp/DPGrimsonGMMBGS.h \
    package_bgs/dp/DPMeanBGS.h \
    package_bgs/dp/DPPratiMediodBGS.h \
    package_bgs/dp/DPTextureBGS.h \
    package_bgs/dp/DPWrenGABGS.h \
    package_bgs/dp/DPZivkovicAGMMBGS.h \
    package_bgs/dp/Eigenbackground.h \
    package_bgs/dp/Error.h \
    package_bgs/dp/GrimsonGMM.h \
    package_bgs/dp/Image.h \
    package_bgs/dp/MeanBGS.h \
    package_bgs/dp/PratiMediodBGS.h \
    package_bgs/dp/TextureBGS.h \
    package_bgs/dp/WrenGA.h \
    package_bgs/dp/ZivkovicAGMM.h
