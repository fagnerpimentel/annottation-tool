/*
This file is part of BGSLibrary.

BGSLibrary is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BGSLibrary is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with BGSLibrary.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "DPEigenbackgroundBGS.h"

DPEigenbackgroundBGS::DPEigenbackgroundBGS() : firstTime(true), frameNumber(0), showOutput(true), threshold(225), historySize(20), embeddedDim(10)
{
  std::cout << "DPEigenbackgroundBGS()" << std::endl;
}

DPEigenbackgroundBGS::~DPEigenbackgroundBGS()
{
  std::cout << "~DPEigenbackgroundBGS()" << std::endl;
}

void DPEigenbackgroundBGS::process(const cv::Mat &img_input, cv::Mat &img_output, cv::Mat &img_bgmodel)
{
  if(img_input.empty())
    return;

  loadConfig();

  if(firstTime)
    saveConfig();

  frame = new IplImage(img_input);
  
  if(firstTime)
    frame_data.ReleaseMemory(false);
  frame_data = frame;

  if(firstTime)
  {
    int width	= img_input.size().width;
    int height = img_input.size().height;

    lowThresholdMask = cvCreateImage(cvSize(width, height), IPL_DEPTH_8U, 1);
    lowThresholdMask.Ptr()->origin = IPL_ORIGIN_BL;

    highThresholdMask = cvCreateImage(cvSize(width, height), IPL_DEPTH_8U, 1);
    highThresholdMask.Ptr()->origin = IPL_ORIGIN_BL;

    params.SetFrameSize(width, height);
    params.LowThreshold() = 500;
    params.HighThreshold() = 10000;	// Note: high threshold is used by post-processing
    //params.LowThreshold() = threshold; //15*15;
    //params.HighThreshold() = 2*params.LowThreshold();	// Note: high threshold is used by post-processing
    //params.HistorySize() = 100;
    params.HistorySize() = historySize;
    //params.EmbeddedDim() = 20;
    params.EmbeddedDim() = embeddedDim;

    bgs.Initalize(params);
    bgs.InitModel(frame_data);
  }

  bgs.Subtract(frameNumber, frame_data, lowThresholdMask, highThresholdMask);

  ////////////////////////
  cv::Mat low(lowThresholdMask.Ptr());
  cv::Mat high(highThresholdMask.Ptr());

  //cv::imshow("low", low);
  //cv::imshow("high", high);

  int width	= img_input.size().width;
  int height = img_input.size().height;

  BwImage histeresys = cvCreateImage(cvSize(width, height), IPL_DEPTH_8U, 1);
  histeresys.Ptr()->origin = IPL_ORIGIN_BL;
  for(unsigned int r = 0; r < height; ++r){
  for(unsigned int c = 0; c < width; ++c){
      histeresys(r,c) = 0;
  }}
  for(unsigned int r = 0; r < height; ++r){
  for(unsigned int c = 0; c < width; ++c){
      if(highThresholdMask(r,c) == 255)
          thresholdWithHisteresys(histeresys, r, c, height, width);
  }}

  cv::Mat foregroun_histeresys(histeresys.Ptr());
  //cv::imshow("histeresys", foregroun_histeresys);

  ////////////////////


  lowThresholdMask.Clear();
  bgs.Update(frameNumber, frame_data, lowThresholdMask);
  
  cv::Mat foreground(highThresholdMask.Ptr());

  if(showOutput)
    cv::imshow("Eigenbackground (Oliver)", foreground);

//  foreground.copyTo(img_output);
  foregroun_histeresys.copyTo(img_output);

  delete frame;
  firstTime = false;
  frameNumber++;
}

void DPEigenbackgroundBGS::saveConfig()
{
  CvFileStorage* fs = cvOpenFileStorage("./config/DPEigenbackgroundBGS.xml", 0, CV_STORAGE_WRITE);

  cvWriteInt(fs, "threshold", threshold);
  cvWriteInt(fs, "historySize", historySize);
  cvWriteInt(fs, "embeddedDim", embeddedDim);
  cvWriteInt(fs, "showOutput", showOutput);

  cvReleaseFileStorage(&fs);
}

void DPEigenbackgroundBGS::loadConfig()
{
  CvFileStorage* fs = cvOpenFileStorage("./config/DPEigenbackgroundBGS.xml", 0, CV_STORAGE_READ);
  
  threshold = cvReadIntByName(fs, 0, "threshold", 225);
  historySize = cvReadIntByName(fs, 0, "historySize", 20);
  embeddedDim = cvReadIntByName(fs, 0, "embeddedDim", 10);
  showOutput = cvReadIntByName(fs, 0, "showOutput", true);

  cvReleaseFileStorage(&fs);
}

void DPEigenbackgroundBGS::thresholdWithHisteresys(BwImage& histeresys, int I,int J, int h, int w){
    if(I<1 || I>h || J<1 || J>w){
        return;
    }

    if(histeresys(I,J) == 0){
    if(lowThresholdMask(I,J) == 255){
            histeresys(I,J) = 255;

            thresholdWithHisteresys(histeresys, I-1,J  ,h ,w);
            thresholdWithHisteresys(histeresys, I  ,J+1,h ,w);
            thresholdWithHisteresys(histeresys, I+1,J  ,h ,w);
            thresholdWithHisteresys(histeresys, I  ,J-1,h ,w);

    }}
}
