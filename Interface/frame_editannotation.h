#ifndef FRAME_EDITANNOTATION_H
#define FRAME_EDITANNOTATION_H

#include <cv.h>
#include <QFrame>
#include <QDebug>
#include <QMessageBox>
#include <QDir>
#include <QShortcut>

#include "acquisition.h"
#include "label_draw.h"
#include "util.h"

namespace Ui {
class Frame_EditAnnotation;
}

class Frame_EditAnnotation : public QFrame
{
    Q_OBJECT
    
public:
    explicit Frame_EditAnnotation(QWidget *parent = 0);
    ~Frame_EditAnnotation();

    void load();

//signals:
//    void signal_loadAnnotation(Acquisition*, Acquisition*);

private slots:
    void on_forwardBtn_clicked();
    void on_backwardBtn_clicked();
    void on_ForegroundBtn_clicked();
    void on_BackgroundBtn_clicked();
    void on_brush_down_clicked();
    void on_brush_up_clicked();
//    void on_load_clicked();
    void on_setFrame_clicked();
//    void slot_loadAnnotation(Acquisition *captureInput, Acquisition *captureAnnotation);

    void on_changeView_clicked();

private:
    Ui::Frame_EditAnnotation *ui;
    Label_Draw *frame;
    int framePosition;
    int num_frames;

//    Acquisition *captureInput;
//    std::vector<cv::Mat> *vectorInput;
    std::vector<cv::Mat> *vectorAnnotation;

};

#endif // FRAME_EDITANNOTATION_H
