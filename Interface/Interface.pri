INCLUDEPATH += $$PWD
DEPENDPATH += $$PWD
SOURCES += \  
    Interface/mainwindow.cpp \
    Interface/frame_preannotaton.cpp \
    Interface/frame_selectroi.cpp \
    Interface/frame_editannotation.cpp \
    Interface/label_draw.cpp \
    Interface/frame_createannotation.cpp \
    Interface/label_selecroi.cpp
HEADERS += \  
    Interface/mainwindow.h \
    Interface/frame_preannotaton.h \
    Interface/frame_selectroi.h \
    Interface/frame_editannotation.h \
    Interface/label_draw.h \
    Interface/frame_createannotation.h \
    Interface/label_selecroi.h

FORMS += \
    Interface/frame_preannotaton.ui \
    Interface/frame_editannotation.ui \
    Interface/frame_createannotation.ui
