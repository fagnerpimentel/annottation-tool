#include "frame_selectroi.h"

Frame_SelectRoi::Frame_SelectRoi(QWidget *parent) :
    QFrame(parent)
{
    this->resize(830, 530);
    this->setFrameShape(QFrame::StyledPanel);
    this->setFrameShadow(QFrame::Raised);
    imageRoi = new Label_SelecRoi(this);
    imageRoi->setGeometry(QRect(30, 30, 640, 480));
    clearBtn = new QPushButton(this);
    clearBtn->setGeometry(QRect(700, 30, 100, 30));
    clearBtn->setText("Clear");
    saveBtn = new QPushButton(this);
    saveBtn->setGeometry(QRect(700, 90, 100, 30));
    saveBtn->setText("Save");

    connect(clearBtn, SIGNAL(clicked()),
            this, SLOT(on_clearBtn_clicked()));
    connect(saveBtn, SIGNAL(clicked()),
            this, SLOT(on_saveBtn_clicked()));

}

Frame_SelectRoi::~Frame_SelectRoi()
{        
    delete imageRoi;
    delete clearBtn;
    delete saveBtn;
}

void
Frame_SelectRoi::loadImage(cv::Mat image)
{
    imageRoi->loadImage(image);
}

void
Frame_SelectRoi::on_saveBtn_clicked()
{
    emit sendRoi(imageRoi->roiMask);
    this->close();
}
void Frame_SelectRoi::on_clearBtn_clicked()
{
    imageRoi->ROI_Vertices->clear();
    imageRoi->roiMask->setTo(cv::Scalar(0,0,0));
    imageRoi->showImage();
}
