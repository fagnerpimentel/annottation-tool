#ifndef FRAME_PREANNOTATON_H
#define FRAME_PREANNOTATON_H

#include <QFrame>
#include <QFileDialog>
#include <QMessageBox>
#include <QDir>

#include "acquisition.h"
#include "frame_selectroi.h"
#include "mainwindow.h"
#include "../package_bgs/IBGS.h"
#include "../package_bgs/dp/DPEigenbackgroundBGS.h"

#include "../util.h"

namespace Ui {
class Frame_PreAnnotaton;
}

class Frame_PreAnnotaton : public QFrame
{
    Q_OBJECT
    
public:
    explicit Frame_PreAnnotaton(QWidget *parent = 0);
    ~Frame_PreAnnotaton();

//    void load(std::vector<cv::Mat> *vi, std::vector<cv::Mat> *va);

    void ShowImages(QImage frame_in, QImage frame_out);
    void progressBarUpdate(int value);

private slots:
    void on_StartBtn_clicked();
    void on_roiBtn_clicked();
    void frameCountSlot(int count);
    void catchRoi(cv::Mat *roi);

    void on_loadVideo_clicked();

signals:
   void frameCountSignal(int count);

private:
    Ui::Frame_PreAnnotaton *ui;

    Frame_SelectRoi *sr;
    cv::Mat *roiMask;
    IBGS *bgs;

    Acquisition *videoInput;
//    std::vector<cv::Mat> *vectorInput;
//    std::vector<cv::Mat> *vectorAnnotation;
};

#endif // FRAME_PREANNOTATON_H
