#include "label_draw.h"

Label_Draw::Label_Draw(QWidget *parent) :
    QLabel(parent)
{
    brushType = 255;
    brushSize = 5;
    setMouseTracking(true);

    view = 1;
}

void
Label_Draw::load(int framePosition)
{
    std::stringstream ss("");
    ss << std::setw(4) << std::setfill('0') << framePosition;
    std::string fileNumber = ss.str();
    std::string fileNameIn = "input/input"+fileNumber+".jpg";
    std::string fileNameGt = "groundtruth/groundtruth"+fileNumber+".jpg";

    input = cv::imread(fileNameIn, CV_LOAD_IMAGE_COLOR);
    cv::cvtColor(input, input, CV_BGR2RGB);
    annotation = cv::imread(fileNameGt, CV_LOAD_IMAGE_GRAYSCALE);

    mouseMask = cv::Mat(annotation.rows, annotation.cols, CV_8UC1);
    mouseMask.setTo(cv::Scalar(255,255,255));

    showAnnotation();

}

void
Label_Draw::save(int framePosition)
{
    std::stringstream ss("");
    ss << std::setw(4) << std::setfill('0') << framePosition;
    std::string fileNumber = ss.str();
    std::string fileName = "groundtruth/groundtruth"+fileNumber+".jpg";
    cv::imwrite(fileName, annotation);
}

void
Label_Draw::mouseMoveEvent(QMouseEvent *event) {

    mouseMask.setTo(cv::Scalar(255,255,255));

    QPoint point = event->pos();
    int pos_x = (mouseMask.cols * point.rx()) /this->size().width();
    int pos_y = (mouseMask.rows * point.ry()) /this->size().height();
    cv::circle(mouseMask, cv::Point(pos_x, pos_y), brushSize,
               cv::Scalar(0,0,0),1);

    if(event->buttons() == Qt::LeftButton)
        cv::circle(annotation, cv::Point(pos_x, pos_y), brushSize,
                   cv::Scalar(brushType,brushType,brushType),-1);

    showAnnotation();

    QLabel::mouseMoveEvent(event);
}


void
Label_Draw::showAnnotation()
{
    cv::Mat result;

    std::vector<cv::Mat> RGB;
    split(input, RGB);

    for (int i = 0; i < annotation.rows; ++i)
    for (int j = 0; j < annotation.cols; ++j) {

        if(view == 1){
            uchar data1 = mouseMask.at<uchar>(i,j);
            if((int)data1 == 0) {
                RGB[0].at<uchar>(i,j) = 0;
                RGB[1].at<uchar>(i,j) = 0;
                RGB[2].at<uchar>(i,j) = 0;
            }
            uchar data2 = annotation.at<uchar>(i,j);
            if((int)data2 > 128) {
                RGB[0].at<uchar>(i,j) = (int)RGB[0].at<uchar>(i,j) * 1;
                RGB[1].at<uchar>(i,j) = (int)RGB[1].at<uchar>(i,j) * 0.7;
                RGB[2].at<uchar>(i,j) = (int)RGB[2].at<uchar>(i,j) * 0.7;
            }
        }
        else{
            uchar data2 = annotation.at<uchar>(i,j);
            RGB[0].at<uchar>(i,j) = data2;
            RGB[1].at<uchar>(i,j) = data2;
            RGB[2].at<uchar>(i,j) = data2;

            uchar data1 = mouseMask.at<uchar>(i,j);
            if((int)data1 == 0) {
                RGB[0].at<uchar>(i,j) = 255;
                RGB[1].at<uchar>(i,j) = 0;
                RGB[2].at<uchar>(i,j) = 0;
            }

        }

    }
    cv::merge(RGB, result);

    setPixmap(QPixmap::fromImage(Mat2QImage(result).scaled(this->size())));
}

void
Label_Draw::setBrushBackground()
{
    brushType = 0;
}

void
Label_Draw::setBrushForeground()
{
    brushType = 255;
}

void
Label_Draw::changeView()
{
    view = view*(-1);
    showAnnotation();
}
