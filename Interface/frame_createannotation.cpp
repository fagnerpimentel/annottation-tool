#include "frame_createannotation.h"
#include "ui_frame_createannotation.h"

Frame_CreateAnnotation::Frame_CreateAnnotation(QWidget *parent) :
    QFrame(parent),
    ui(new Ui::Frame_CreateAnnotation)
{
    ui->setupUi(this);


    connect(this, SIGNAL(signal_saveAnnotation(cv::VideoWriter*,cv::VideoWriter*)),
            this, SLOT(slot_saveAnnotation(cv::VideoWriter*,cv::VideoWriter*)));
}

Frame_CreateAnnotation::~Frame_CreateAnnotation()
{
    delete ui;
}

void
Frame_CreateAnnotation::load(Acquisition *ci, std::vector<cv::Mat> *va)
{
    captureInput = ci;
//    VectorInput = vi;
    VectorAnnotation = va;
}

void Frame_CreateAnnotation::on_StartBtn_clicked()
{
//    Acquisition *videoInput = new Acquisition("Videos/input.avi");
//    if(!videoInput->isOpened()){
//        QMessageBox messageBox;
//        messageBox.critical(0,"Error","Cannot open input file!");
//    }
    cv::VideoWriter *bm = new cv::VideoWriter("Videos/binarymask1.avi",
                                              captureInput->getFOURCC() , captureInput->getFPS(), captureInput->size());
    cv::VideoWriter *bb = new cv::VideoWriter("Videos/boundingbox1.avi",
                                              captureInput->getFOURCC() , captureInput->getFPS(), captureInput->size());

//    captureInput->getFrame(0);
////    emit signal_saveAnnotation(bm, bb);
//    slot_saveAnnotation(bm, bb);

    int k = 0;
    while(true){

        ui->progressBar->setValue(k/(int)VectorAnnotation->size()*100);

        cv::Mat binarymask = VectorAnnotation->at(k).clone();
        cv::Mat boundingbox = cv::Mat::zeros( binarymask.size(), CV_8UC3 );

        std::vector<std::vector<cv::Point> > contours;
        std::vector<cv::Vec4i> hierarchy;

        /// Find contours
        cv::findContours( binarymask, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, cv::Point(0, 0) );

        /// Approximate contours to polygons + get bounding rects and circles
        std::vector<std::vector<cv::Point> > contours_poly( contours.size() );
        std::vector<cv::Rect> boundRect( contours.size() );
        std::vector<cv::Point2f>center( contours.size() );
        std::vector<float>radius( contours.size() );

        for( int i = 0; i < contours.size(); i++ ){
            cv::approxPolyDP( cv::Mat(contours[i]), contours_poly[i], 3, true );
            boundRect[i] = cv::boundingRect( cv::Mat(contours_poly[i]) );
            cv::minEnclosingCircle( (cv::Mat)contours_poly[i], center[i], radius[i] );
        }

        /// Draw polygonal contour + bonding rects + circles
        for( int i = 0; i< contours.size(); i++ ){
            cv::Scalar color = cv::Scalar( 255, 255, 255 );
            cv::rectangle( boundingbox, boundRect[i].tl(), boundRect[i].br(), color, -1);
        }

        binarymask = VectorAnnotation->at(k).clone();
        cv::cvtColor(binarymask, binarymask, CV_GRAY2RGB);
        ui->BMView->setPixmap(QPixmap::fromImage(Mat2QImage(binarymask).scaled(ui->BMView->size())));
        ui->BBView->setPixmap(QPixmap::fromImage(Mat2QImage(boundingbox).scaled(ui->BBView->size())));

        bm->write(binarymask);
        bb->write(boundingbox);

        cv::waitKey(1);
        k++;
        if(k == (int)VectorAnnotation->size())
            break;
    }
    bm->release();
    bb->release();

    QMessageBox msgBox;
    msgBox.setText("Done.");
    msgBox.exec();
}

void
Frame_CreateAnnotation::slot_saveAnnotation(cv::VideoWriter *bm, cv::VideoWriter *bb ){

    if (captureInput->getPos() == captureInput->getFrameCount()){
        bm->release();
        bb->release();
        return;
    }

    ui->progressBar->setValue((captureInput->getPos())/captureInput->getFrameCount()*100);
    captureInput->update();

    cv::Mat binarymask = VectorAnnotation->at(captureInput->getPos()).clone();
    cv::Mat boundingbox = cv::Mat::zeros( binarymask.size(), CV_8UC3 );

    std::vector<std::vector<cv::Point> > contours;
    std::vector<cv::Vec4i> hierarchy;

    /// Find contours
    cv::findContours( binarymask, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, cv::Point(0, 0) );

    /// Approximate contours to polygons + get bounding rects and circles
    std::vector<std::vector<cv::Point> > contours_poly( contours.size() );
    std::vector<cv::Rect> boundRect( contours.size() );
    std::vector<cv::Point2f>center( contours.size() );
    std::vector<float>radius( contours.size() );

    for( int i = 0; i < contours.size(); i++ ){
        cv::approxPolyDP( cv::Mat(contours[i]), contours_poly[i], 3, true );
        boundRect[i] = cv::boundingRect( cv::Mat(contours_poly[i]) );
        cv::minEnclosingCircle( (cv::Mat)contours_poly[i], center[i], radius[i] );
    }

    /// Draw polygonal contour + bonding rects + circles
    for( int i = 0; i< contours.size(); i++ ){
        cv::Scalar color = cv::Scalar( 255, 255, 255 );
        cv::rectangle( boundingbox, boundRect[i].tl(), boundRect[i].br(), color, -1);
    }

    binarymask = VectorAnnotation->at(captureInput->getPos()).clone();
    cv::cvtColor(binarymask, binarymask, CV_GRAY2RGB);
    ui->BMView->setPixmap(QPixmap::fromImage(Mat2QImage(binarymask).scaled(ui->BMView->size())));
    ui->BBView->setPixmap(QPixmap::fromImage(Mat2QImage(boundingbox).scaled(ui->BBView->size())));

    bm->write(binarymask);
    bb->write(boundingbox);

    cv::waitKey(1);
//    emit signal_saveAnnotation(bm, bb);
    slot_saveAnnotation(bm, bb);

}
