#ifndef FRAME_SELECTROI_H
#define FRAME_SELECTROI_H

#include <QFrame>
#include <QPushButton>
#include "label_selecroi.h"

class Frame_SelectRoi : public QFrame
{
    Q_OBJECT
    
public:
    explicit Frame_SelectRoi(QWidget *parent = 0);
    ~Frame_SelectRoi();
    void loadImage(cv::Mat image);

private slots:
    void on_clearBtn_clicked();
    void on_saveBtn_clicked();

signals:
    void sendRoi(cv::Mat *roi);

private:    
    Label_SelecRoi *imageRoi;
    QPushButton *clearBtn;
    QPushButton *saveBtn;

};

#endif // FRAME_SELECTROI_H
