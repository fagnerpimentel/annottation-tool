#include "frame_preannotaton.h"
#include "ui_frame_preannotaton.h"

#include "../util.h"

Frame_PreAnnotaton::Frame_PreAnnotaton(QWidget *parent) :
    QFrame(parent),
    ui(new Ui::Frame_PreAnnotaton)
{
    ui->setupUi(this);
    bgs = new DPEigenbackgroundBGS;

}

Frame_PreAnnotaton::~Frame_PreAnnotaton()
{
    delete ui;
    delete sr;
    delete bgs;
    delete roiMask;
    delete videoInput;
//    delete vectorInput;
//    delete vectorAnnotation;
}

//void
//Frame_PreAnnotaton::load(std::vector<cv::Mat> *vi, std::vector<cv::Mat> *va)
//{
//    vectorInput = vi;
//    vectorAnnotation = va;
//}

void
Frame_PreAnnotaton::ShowImages(QImage frame_in, QImage frame_out){
    ui->inputView->setPixmap(QPixmap::fromImage(frame_in.scaled(ui->inputView->size())));
    ui->outputView->setPixmap(QPixmap::fromImage(frame_out.scaled(ui->inputView->size())));
}

void Frame_PreAnnotaton::on_roiBtn_clicked()
{

    cv::Mat frame = videoInput->getFrame();

    sr = new Frame_SelectRoi();
    sr->setAttribute(Qt::WA_DeleteOnClose, true);
    sr->loadImage(frame);
    sr->show();

    connect(sr, SIGNAL(sendRoi(cv::Mat*)),
            this, SLOT(catchRoi(cv::Mat*)));
}

void Frame_PreAnnotaton::on_StartBtn_clicked()
{
    cv::Mat img_input;
    cv::Mat img_mask;
    cv::Mat img_bkgmodel;
    cv::Mat imageDest;
    cv::Mat imagefinal;


    std::stringstream ss;
    std::string fileNumber;
    std::string fileNameIn;
    std::string fileNameGt;

    int max = videoInput->getFrameCount();
//    int max = 200;
    int k = 0;
    while(k < max){

        emit frameCountSignal(k/max*100);

        /* imput */
        img_input = videoInput->getFrame().clone();

//        img_input = Equalization(img_input);

        // Cut out ROI and store it in imageDest
        img_input.copyTo(imageDest, *roiMask);

        /* bgs */
        bgs->process(imageDest, img_mask, img_bkgmodel);

        /// Apply the erosion operation
        cv::Mat element = cv::getStructuringElement( cv::MORPH_ELLIPSE, cv::Size(3, 3));
        cv::dilate(img_mask, img_mask, element );
        cv::erode( img_mask, img_mask, element );

        cv::cvtColor(img_input, img_input, CV_BGRA2RGB);
        ss.str("");
        ss << std::setw(4) << std::setfill('0') << k;
        fileNumber = ss.str();
        fileNameIn = "input/input"+fileNumber+".jpg";
        fileNameGt = "groundtruth/groundtruth"+fileNumber+".jpg";
        cv::imwrite(fileNameIn, img_input);
        cv::imwrite(fileNameGt, img_mask);
        cv::cvtColor(img_input, img_input, CV_RGBA2BGR);

        cv::cvtColor(img_mask, imagefinal, CV_GRAY2RGB);

        ShowImages(Mat2QImage(img_input), Mat2QImage(imagefinal));

        cvWaitKey(1);
        videoInput->update();
        k++;
    }
}

void Frame_PreAnnotaton::progressBarUpdate(int value)
{
    ui->progressBar->setValue(value);
}

void Frame_PreAnnotaton::frameCountSlot(int count)
{
    ui->progressBar->setValue(count);
}

void Frame_PreAnnotaton::catchRoi(cv::Mat *roi)
{
    roiMask = roi;
}

void Frame_PreAnnotaton::on_loadVideo_clicked()
{
    QString path = QFileDialog::getOpenFileName();
    ui->textBrowser->setText(path);

    videoInput = new Acquisition(path.toStdString());
    bool open = videoInput->isOpened();
    if(open){
        videoInput->update();
        cv::Mat frame = videoInput->getFrame().clone();
        roiMask = new cv::Mat(frame.rows, frame.cols,
                              CV_8UC1, cv::Scalar(255,255,255));

        ShowImages(Mat2QImage(frame), Mat2QImage(frame));
        ui->StartBtn->setEnabled(true);
        ui->roiBtn->setEnabled(true);
    }else{
        QMessageBox::information(this,"Error!","Cannot open input file!");
    }
}
