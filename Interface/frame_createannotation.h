#ifndef FRAME_CREATEANNOTATION_H
#define FRAME_CREATEANNOTATION_H

#include <QFrame>
#include <QMessageBox>

#include <cv.h>

#include "acquisition.h"
#include "util.h"

namespace Ui {
class Frame_CreateAnnotation;
}

class Frame_CreateAnnotation : public QFrame
{
    Q_OBJECT
    
public:
    explicit Frame_CreateAnnotation(QWidget *parent = 0);
    ~Frame_CreateAnnotation();

    void load(Acquisition *ci, std::vector<cv::Mat> *va);

signals:
    void signal_saveAnnotation(cv::VideoWriter *bm, cv::VideoWriter *bb );

private slots:
    void on_StartBtn_clicked();
    void slot_saveAnnotation(cv::VideoWriter *bm, cv::VideoWriter *bb );

private:
    Ui::Frame_CreateAnnotation *ui;

    Acquisition *captureInput;
//    std::vector<cv::Mat> *VectorInput;
    std::vector<cv::Mat> *VectorAnnotation;
};

#endif // FRAME_CREATEANNOTATION_H
