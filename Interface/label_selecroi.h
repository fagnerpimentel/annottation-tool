#ifndef LABEL_SELECROI_H
#define LABEL_SELECROI_H

#include <QLabel>
#include <QMouseEvent>
//#include <QEvent>
#include <cv.h>
#include "util.h"

class Label_SelecRoi: public QLabel
{
    Q_OBJECT
public:
    explicit Label_SelecRoi(QWidget *parent = 0);

    void mousePressEvent(QMouseEvent *ev);
    void loadImage(cv::Mat image);
    void showImage();

    int x, y;
    cv::Mat frame;
    cv::Mat *roiMask;
    std::vector<cv::Point> *ROI_Vertices;


signals:
    void Mouse_Pressed();

};

#endif // LABEL_SELECROI_H
