#include "label_selecroi.h"

Label_SelecRoi::Label_SelecRoi(QWidget *parent) :
    QLabel(parent)
{
    ROI_Vertices = new std::vector<cv::Point>();
}

void
Label_SelecRoi::mousePressEvent(QMouseEvent *event)
{
    QPoint point = event->pos();
    int pos_x = (roiMask->cols * point.rx()) /this->size().width();
    int pos_y = (roiMask->rows * point.ry()) /this->size().height();
    roiMask->setTo(cv::Scalar(0,0,0));
    cv::circle(*roiMask, cv::Point(pos_x, pos_y), 1,
               cv::Scalar(255,255,255),-1);

    ROI_Vertices->push_back(cv::Point(pos_x,pos_y));

    showImage();

    QLabel::mousePressEvent(event);
}

void
Label_SelecRoi::showImage()
{

    if(ROI_Vertices->size()>=3){
        // Create Polygon from vertices
        std::vector<cv::Point> ROI_Poly;
        cv::approxPolyDP(*ROI_Vertices, ROI_Poly, 1.0, true);
        cv::fillConvexPoly(*roiMask, &ROI_Poly[0], ROI_Poly.size(), 255);
    }

    cv::Mat result;
    std::vector<cv::Mat> RGB;
    split(frame, RGB);
    for (int i = 0; i < frame.rows; ++i)
    for (int j = 0; j < frame.cols; ++j) {
        uchar data1 = roiMask->at<uchar>(i,j);
        if((int)data1 == 255) {
            RGB[0].at<uchar>(i,j) = RGB[0].at<uchar>(i,j) * 0;
            RGB[1].at<uchar>(i,j) = RGB[1].at<uchar>(i,j) * 1;
            RGB[2].at<uchar>(i,j) = RGB[2].at<uchar>(i,j) * 0;
        }
    }
    cv::merge(RGB, result);

    setPixmap(QPixmap::fromImage(Mat2QImage(result).scaled(this->size())));

}

void
Label_SelecRoi::loadImage(cv::Mat image)
{
    frame = image;
    roiMask = new cv::Mat(frame.rows, frame.cols, CV_8UC1);
    roiMask->setTo(cv::Scalar(255,255,255));
    showImage();

}
