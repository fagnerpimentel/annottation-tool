#ifndef LABEL_DRAW_H
#define LABEL_DRAW_H

#include <QLabel>
#include <QMouseEvent>
#include <cv.h>
#include "acquisition.h"
#include "util.h"

class Label_Draw: public QLabel
{
    Q_OBJECT
public:
    explicit Label_Draw(QWidget *parent = 0);

    void load(int framePosition);
    void save(int framePosition);

    void showAnnotation();

    void mouseMoveEvent(QMouseEvent *ev);

    void setBrushBackground();
    void setBrushForeground();

    void changeView();

    int x, y;
    int brushType;
    int brushSize;

private:
    cv::Mat input;
    cv::Mat annotation;
    cv::Mat mouseMask;

    int view;


signals:
    void Mouse_Pressed();

public slots:

};

#endif // LABEL_DRAW_H

