#include "frame_editannotation.h"
#include "ui_frame_editannotation.h"

Frame_EditAnnotation::Frame_EditAnnotation(QWidget *parent) :
    QFrame(parent),
    ui(new Ui::Frame_EditAnnotation)
{
    ui->setupUi(this);
    framePosition = 0;
    frame = new Label_Draw(this);
    frame->setGeometry(QRect(30, 30, 960, 720));
    ui->brush_lcd->display(frame->brushSize);

    num_frames = QDir("input/").count()-2;

    QShortcut *shortcut_bg = new QShortcut(QKeySequence("b"), this);
    connect(shortcut_bg, SIGNAL(activated()), this, SLOT(on_BackgroundBtn_clicked()));

    QShortcut *shortcut_fg = new QShortcut(QKeySequence("f"), this);
    connect(shortcut_fg, SIGNAL(activated()), this, SLOT(on_ForegroundBtn_clicked()));

    QShortcut *shortcut_left = new QShortcut(QKeySequence("Left"), this);
    connect(shortcut_left, SIGNAL(activated()), this, SLOT(on_backwardBtn_clicked()));

    QShortcut *shortcut_right = new QShortcut(QKeySequence("Right"), this);
    connect(shortcut_right, SIGNAL(activated()), this, SLOT(on_forwardBtn_clicked()));

    QShortcut *shortcut_up = new QShortcut(QKeySequence("Up"), this);
    connect(shortcut_up, SIGNAL(activated()), this, SLOT(on_brush_up_clicked()));

    QShortcut *shortcut_down = new QShortcut(QKeySequence("Down"), this);
    connect(shortcut_down, SIGNAL(activated()), this, SLOT(on_brush_down_clicked()));

    QShortcut *change_view = new QShortcut(QKeySequence("c"), this);
    connect(change_view, SIGNAL(activated()), this, SLOT(on_changeView_clicked()));
}

Frame_EditAnnotation::~Frame_EditAnnotation()
{
    delete ui;
}

void
Frame_EditAnnotation::load()
{
    QDir DirIN("input/");
    if(DirIN.count()-2 == 0)
    {
//        QMessageBox::information(this,"Empty!!!","Input directory is empty");
        return;
    }
    QDir DirGT("groundtruth/");
    if(DirGT.count()-2 == 0)
    {
//        QMessageBox::information(this,"Empty!!!","Groundtruth directory is empty");
        return;
    }

    frame->load(framePosition);

    ui->lcdNumber_cont->display(framePosition);
    ui->lcdNumber_total->display(num_frames-1);

}

void
Frame_EditAnnotation::on_forwardBtn_clicked()
{
    frame->save(framePosition);
    framePosition++;
    if(framePosition >= num_frames)
        framePosition = num_frames-1;
    frame->load(framePosition);

//    frame->showAnnotation();
    ui->lcdNumber_cont->display(framePosition);
    ui->lcdNumber_total->display(num_frames-1);
}

void Frame_EditAnnotation::on_backwardBtn_clicked()
{
    frame->save(framePosition);
    framePosition--;
    if(framePosition < 0)
        framePosition = 0;
    frame->load(framePosition);

//    frame->showAnnotation();
    ui->lcdNumber_cont->display(framePosition);
    ui->lcdNumber_total->display(num_frames-1);

}

void Frame_EditAnnotation::on_ForegroundBtn_clicked()
{
    ui->BackgroundBtn->setEnabled(true);
    ui->ForegroundBtn->setEnabled(false);
    frame->setBrushForeground();
}

void Frame_EditAnnotation::on_BackgroundBtn_clicked()
{
    ui->BackgroundBtn->setEnabled(false);
    ui->ForegroundBtn->setEnabled(true);
    frame->setBrushBackground();
}

void Frame_EditAnnotation::on_brush_down_clicked()
{
    frame->brushSize--;
    if(frame->brushSize < 1)
        frame->brushSize = 1;

    frame->showAnnotation();
    ui->brush_lcd->display(frame->brushSize);
}

void Frame_EditAnnotation::on_brush_up_clicked()
{
    frame->brushSize++;
    if(frame->brushSize > 50)
        frame->brushSize = 50;

    frame->showAnnotation();
    ui->brush_lcd->display(frame->brushSize);
}


void
Frame_EditAnnotation::on_setFrame_clicked()
{
    frame->save(framePosition);
    framePosition = ui->lineEdit->text().toInt();

    if(framePosition >= num_frames)
        framePosition = num_frames-1;
    frame->load(framePosition);

    ui->lcdNumber_cont->display(framePosition);
    ui->lcdNumber_total->display(num_frames-1);

}

void Frame_EditAnnotation::on_changeView_clicked()
{
    frame->changeView();
}
