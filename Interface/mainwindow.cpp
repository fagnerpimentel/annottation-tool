#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent)
{
    this->resize(1300, 800);

//    std::vector<cv::Mat> *vectorInput = new std::vector<cv::Mat>;
//    std::vector<cv::Mat> *vectorAnnotation = new std::vector<cv::Mat>;

    QWidget *centralWidget = new QWidget(this);
    this->setCentralWidget(centralWidget);

    QMenuBar *menuBar = new QMenuBar(this);
    this->setMenuBar(menuBar);

    QStatusBar *statusBar = new QStatusBar(this);
    this->setStatusBar(statusBar);

    QTabWidget *tabWidget = new QTabWidget(centralWidget);
    tabWidget->resize(this->size());

    Frame_PreAnnotaton *pa = new Frame_PreAnnotaton(this);
//    pa->load(vectorInput, vectorAnnotation);
    tabWidget->addTab(pa, "Pre Annotation");

    Frame_EditAnnotation *ea = new Frame_EditAnnotation(this);
    ea->load();
    tabWidget->addTab(ea, "Edit Annotation");

//    Frame_CreateAnnotation *ca = new Frame_CreateAnnotation(this);
//    ca->load(captureInput, vectorAnnotation);
//    tabWidget->addTab(ca, "Create Annotation");

}

MainWindow::~MainWindow()
{
}

