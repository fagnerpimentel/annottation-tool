#ifndef UTIL_H
#define UTIL_H

#include <cv.h>
#include <highgui.h>

#include <QImage>

QImage Mat2QImage(cv::Mat &src);
cv::Mat Equalization(const cv::Mat& inputImage);

#endif // UTIL_H
