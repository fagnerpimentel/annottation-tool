#ifndef ACQUISITION_H
#define ACQUISITION_H

#include <cv.h>
#include <highgui.h>

class Acquisition
{
public:
    Acquisition(std::string s);

    bool isOpened();
    void update();
    cv::Mat getFrame();
    cv::Mat getFrame(int frameNumber);
    double getFrameCount();
    cv::Size size();
    double getFOURCC();
    double getFPS();
    double getPos();

private:
    cv::VideoCapture* capture;

    cv::Mat frame;
};

#endif // ACQUISITION_H
