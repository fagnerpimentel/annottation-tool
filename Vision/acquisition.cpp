#include "acquisition.h"

Acquisition::Acquisition(std::string s)
{
    capture =  new cv::VideoCapture(s.data());
}
bool
Acquisition::isOpened(){
    return capture->isOpened();
}

void
Acquisition::update(){
    *capture >> frame;
    cv::cvtColor(frame, frame, CV_BGR2RGB);
}

cv::Mat
Acquisition::getFrame(){
    return frame;
}

cv::Mat
Acquisition::getFrame(int frameNumber){
    capture->set (CV_CAP_PROP_POS_FRAMES, (double)frameNumber);
    update();
    return frame;
}

double
Acquisition::getFrameCount(){
    return capture->get(CV_CAP_PROP_FRAME_COUNT);
}

cv::Size
Acquisition::size(){
    return cv::Size(capture->get(CV_CAP_PROP_FRAME_WIDTH), capture->get(CV_CAP_PROP_FRAME_HEIGHT));
}

double
Acquisition::getFOURCC(){
    return capture->get(CV_CAP_PROP_FOURCC);
}

double
Acquisition::getFPS(){
    return capture->get(CV_CAP_PROP_FPS);
}
double
Acquisition::getPos(){
    return capture->get(CV_CAP_PROP_POS_FRAMES);
}
